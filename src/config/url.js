const test_url_base = "http://localhost:8080";

//Auth Url
export const googleLogoutURl = test_url_base + "/auth/logout";
export const auth_url = test_url_base + "/auth/google";
export const home_url = test_url_base + "/home";
export const eventbrite_auth_url = test_url_base + "/auth/eventbrite/";
export const meetup_auth_url = test_url_base + "/auth/meetup/";
//Eventbrite Url
export const eventbrite_url = test_url_base + "/eventbrite/user";
//Meetup url
export const meetup_group_url = test_url_base + "/meetup/groups";
export const meetup_group_dashboard = test_url_base + "/meetup/dashboard";
