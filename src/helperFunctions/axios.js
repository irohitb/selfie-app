//Here we are going to make all the network request
import axios from "axios";

const makeAxiosGetRequest = url => {
  return axios.get(url, { withCredentials: true });
};

const makeAxiosParallelRequest = (url1, url2) => {
  console.log(url1, url2);
  let url_1 = axios.get(url1, { withCredentials: true });
  let url_2 = axios.get(url2, { withCredentials: true });
  return Promise.all([url_1, url_2]);
};

export default {
  makeAxiosGetRequest,
  makeAxiosParallelRequest
};
