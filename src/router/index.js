import Vue from "vue";
import VueRouter from "vue-router";
import Meetup from "./../childComponent/meetup.vue";
Vue.use(VueRouter);

export default function(vuexStore) {
  let { store } = vuexStore;

  const ifAuthenticated = (to, from, next) => {
    if (store.getters.GET_USER_AUTHENTICATION) return next();
    else next("/login");
  };

  const ifNotAuthenticated = (to, from, next) => {
    if (!store.getters.GET_USER_AUTHENTICATION) return next();
    else next("/");
  };

  const Router = new VueRouter({
    mode: "history",
    routes: [
      {
        path: "/",
        beforeEnter: ifAuthenticated,
        component: () => {
          return import("./../container/Index.vue");
        },
        children: [
          {
            path: "",
            name: "dashboard",
            component: () => import("./../childComponent/meetup.vue")
          },
          {
            path: "/meetup",
            name: "meetup",
            component: () => import("./../childComponent/meetup.vue")
          },
          {
            path: "/eventbrite",
            name: "eventbrite",
            component: () => import("./../childComponent/eventbrite.vue")
          }
        ]
      },
      {
        path: "/login",
        beforeEnter: ifNotAuthenticated,
        component: () => {
          return import("./../container/logn.vue");
        }
      }
    ],

    // Leave these as is and change from quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE
  });

  return Router;
}
