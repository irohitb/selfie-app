import axiosHelper from "./../../helperFunctions/axios.js";

const meetupState = {
  state: {
    meetupProfileData: null,
    meetupProfileLoading: null,
    meetupProfileError: null
  },
  mutations: {
    SET_MEETUP_DATA_LOADING: state => {
      state.meetupProfileLoading = true;
    },
    SET_MEETUP_DATA: (state, payload) => {
      (state.meetupProfileData = payload),
        (state.meetupProfileLoading = false),
        (state.meetupProfileError = false);
    },
    SET_MEETUP_ERROR: state => {
      (state.meetupProfileLoading = false), (state.meetupProfileError = false);
    }
  },
  actions: {
    MEETUP_PROFILE: async (context, url) => {
      let { meetup_group_url, meetup_group_dashboard } = url;
      context.commit("SET_MEETUP_DATA_LOADING");
      try {
        let data = await axiosHelper.makeAxiosParallelRequest(
          meetup_group_url,
          meetup_group_dashboard
        );
        return context.commit("SET_MEETUP_DATA", data);
      } catch {
        return context.commit("SET_MEETUP_ERROR");
      }
    }
  },
  getters: {
    GET_MEETUP_DATA: state => {
      return state;
    }
  }
};

export default meetupState;
