import axiosHelper from "./../../helperFunctions/axios.js";

const eventBiteState = {
  state: {
    eventbriteProfileData: null,
    eventbriteProfileLoading: null,
    eventbriteProfileError: null
  },
  mutations: {
    SET_EVENTBRITE_DATA_LOADING: state => {
      state.eventbriteProfileLoading = true;
    },
    SET_EVENTBRITE_DATA: (state, payload) => {
      (state.eventbriteProfileData = payload),
        (state.eventbriteProfileLoading = false),
        (state.eventbriteProfileError = false);
    },
    SET_EVENTBRITE_ERROR: state => {
      (state.eventbriteProfileLoading = false),
        (state.eventbriteProfileError = false);
    }
  },
  actions: {
    EVENTBRITE_PROFILE: async (context, url) => {
      console.log(url);
      context.commit("SET_EVENTBRITE_DATA_LOADING");
      try {
        let { data } = await axiosHelper.makeAxiosGetRequest(url);
        return context.commit("SET_EVENTBRITE_DATA", data);
      } catch {
        return context.commit("SET_EVENTBRITE_ERROR");
      }
    }
  },
  getters: {
    GET_EVENTBRITE_DATA: state => {
      return state;
    }
  }
};

export default eventBiteState;
