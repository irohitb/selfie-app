import Vue from 'vue'
import Vuex from 'vuex'
import User from "./user/userState.js"
import Eventbrite from './eventbrite/eventbriteState.js'
import Meetup from "./meetup/meetupState.js"
Vue.use(Vuex)

export default function (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      User,
      Eventbrite,
      Meetup
    }
  })

  return Store
}
