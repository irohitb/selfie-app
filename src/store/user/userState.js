import axiosHelper from "./../../helperFunctions/axios.js";
import { googleLogoutURl } from "./../../config/url";

const userState = {
  state: {
    googleProfileData: null,
    googleProfileLoading: null,
    googleProfileError: null,
    isAuthenticated: false
  },
  mutations: {
    SET_GOOGLEPROFILE_DATA_LOADING: state => {
      state.googleProfileLoading = true;
    },
    SET_GOOGLEPROFILE_DATA: (state, payload) => {
      (state.googleProfileData = payload),
        (state.googleProfileLoading = false),
        (state.googleProfileError = false),
        (state.isAuthenticated = true);
    },
    SET_GOOGLEPROFILE_ERROR: state => {
      (state.googleProfileLoading = false), (state.googleProfileError = false);
    },
    SET_GOOGLEPROFILE_LOGOUT: state => {
      (state.googleProfileData = null),
        (state.isAuthenticated = false),
        (state.googleProfileLoading = false);
    }
  },
  actions: {
    GOOGLE_PROFILE: async (context, url) => {
      context.commit("SET_GOOGLEPROFILE_DATA_LOADING");
      try {
        let { data } = await axiosHelper.makeAxiosGetRequest(url);
        return context.commit("SET_GOOGLEPROFILE_DATA", data);
      } catch {
        return context.commit("SET_GOOGLEPROFILE_ERROR");
      }
    },
    GOOGLE_PROFILE_LOGOUT: async context => {
      context.commit("SET_GOOGLEPROFILE_DATA_LOADING");
      try {
        await axiosHelper.makeAxiosGetRequest(googleLogoutURl);
        return context.commit("SET_GOOGLEPROFILE_LOGOUT");
      } catch {
        return context.commit("SET_GOOGLEPROFILE_ERROR");
      }
    }
  },
  getters: {
    GET_PROFILE_DATA: state => {
      return state;
    },
    GET_USER_AUTHENTICATION: state => {
      return state.isAuthenticated;
    }
  }
};

export default userState;
